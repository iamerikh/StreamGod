import React, { useState, useEffect } from 'react';const Player = ({ y }) => {
    const [x, setX] = useState(100); // Initial x-position
  
    useEffect(() => {
      const driftInterval = setInterval(() => {
        // Update the x-position to create drifting effect
        setX((prevX) => prevX + 5); // Adjust the drift speed as needed
      }, 10); // Adjust the interval to control the smoothness of the drift
  
      return () => {
        clearInterval(driftInterval);
      };
    }, []);
  
    const playerStyle = {
      position: 'absolute',
      left: x + 'px',
      bottom: y + 'px',
    };
  return <div style={playerStyle}>
          <img src="./comet.png" alt="Player" style={{ width: '50px', height: '50px' }} />

  </div>;
};

export default Player;
