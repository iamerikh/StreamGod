import React, { useState, useEffect, useRef } from 'react';
import Player from './player';
import Platform from './plateform';

const Game = () => {
    const [playerY, setPlayerY] = useState(800);
    const [platforms, setPlatforms] = useState([]);
    const [isJumping, setIsJumping] = useState(false);
    const [lastPlatformX, setLastPlatformX] = useState(0); // Move this useState hook outside of generatePlatforms
  
    const gameRef = useRef(null);
  
    // Function to generate platforms
    const generatePlatforms = () => {
      // Calculate the game container width to determine when to add new platforms
      const gameContainerWidth = 800;
  
      // Distance from the right edge of the last platform to the right edge of the game container
      const distanceToNextPlatform = 300;
  
      // Check if the last platform is about to exit the screen
      if (lastPlatformX + distanceToNextPlatform <= gameContainerWidth) {
        // Generate a new platform and add it to the platforms array
        const newPlatform = {
          x: gameContainerWidth,
          y: 350,
          width: 100,
          height: 20,
        };
  
        // Add the new platform to the platforms array
        setPlatforms((prevPlatforms) => [...prevPlatforms, newPlatform]);
  
        // Update the last platform's x-coordinate
        setLastPlatformX(gameContainerWidth);
      }
    };
    
  // Function to move platforms
  const movePlatforms = () => {
    // Logic to move platforms here
    // This is just a basic example; you can implement your own logic to move platforms.
    setPlatforms((prevPlatforms) =>
      prevPlatforms.map((platform) => ({
        ...platform,
        x: platform.x - 5, // Adjust the speed of platform movement
      }))
    );
  };

  // Function to handle jumping and sliding on platforms
  const handlePlayerMovement = (e) => {
    if (e.type === 'mousedown') {
      // When the mouse button is pressed (slide down)
      setIsJumping(false);
    } else if (e.type === 'mouseup') {
      // When the mouse button is released (jump back up)
      setIsJumping(true);
    }
  };

  // Function to check for collisions between player and platforms
  const checkCollisions = () => {
    platforms.forEach((platform) => {
      if (
        playerY + 50 >= platform.y && // Check if player's bottom edge is at or below the platform's top edge
        playerY + 50 <= platform.y + platform.height && // Check if player's bottom edge is at or above the platform's bottom edge
        playerY >= platform.y - 50 && // Check if player's top edge is below the platform's top edge
        playerY <= platform.y + platform.height - 50 // Check if player's top edge is above the platform's bottom edge
      ) {
        setIsJumping(false);
        setPlayerY(platform.y - 50); // Set player's position to be on top of the platform
      }
    });
  };
  // Game loop
  const gameLoop = () => {
    movePlatforms();
    generatePlatforms();
    checkCollisions();
    // Add other game logic here

    // Update player position based on jumping or sliding
    setPlayerY((prevY) => (isJumping ? prevY + 5 : prevY - 5)); // Adjust the speed of jump and slide

    // Request next animation frame
    gameRef.current = requestAnimationFrame(gameLoop);
  };

  useEffect(() => {
    // Start the game loop when the component mounts
    gameRef.current = requestAnimationFrame(gameLoop);

    // Cleanup function to cancel animation frame when the component unmounts
    return () => cancelAnimationFrame(gameRef.current);
  }, []);

  return (
    <div
      style={{ position: 'relative', width: 'auto', height: '600px',backgroundColor: 'black' }}
      onMouseDown={handlePlayerMovement}
      onMouseUp={handlePlayerMovement}
    >
      <Player x={100} y={playerY} />
      {platforms.map((platform, index) => (
        <Platform key={index} {...platform} />
      ))}
    </div>
  );
};

export default Game;