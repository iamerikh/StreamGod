import React from 'react';

const Platform = ({ x, y, width, height }) => {
  const platformStyle = {
    
    width: width + 'px',
    height: height + 'px',
    background: 'green',
    position: 'absolute',
    left: x + 'px',
    bottom: y + 'px',
  };

  return <div style={platformStyle}></div>;
};

export default Platform;
